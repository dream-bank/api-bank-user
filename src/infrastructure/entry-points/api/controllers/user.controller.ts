import {Request, Response} from "express";
import {BaseController} from "./common/base.controller";
import {UserUseCase} from "../../../../domain/use-cases/user/user.usecase";
import {before, POST, route} from "awilix-express";
import {body, validationResult} from 'express-validator';

@route("/api/v1/users")
export class UserController extends BaseController {
    constructor(private readonly userUseCase: UserUseCase) {
        super();
    }

    @POST()
    @before(
        [
            body('name').isString().notEmpty(),
            body('surname').isString().notEmpty(),
            body('identification').isString().notEmpty(),
            body('password').isString().notEmpty()
        ]
    )
    public async createUser(req: Request, res: Response) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }
        try {
            await this.userUseCase.createUser({
                name: req.body.name,
                surname: req.body.surname,
                identification: req.body.identification,
                password: req.body.password,
            } as UserDto);
            res.status(201).send();
        } catch (error) {
            this.handleException(error, res);
        }
    }

    @POST()
    @route("/login")
    @before(
        [
            body('identification').isString().notEmpty(),
            body('password').isString().notEmpty()
        ]
    )
    public async authenticateUser(req: Request, res: Response) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({errors: errors.array()});
            }
            const {identification, password}: UserAuthDto = req.body;
            const result = await this.userUseCase.authenticateUser(identification, password);
            res.json(result);
        } catch (error) {
            this.handleException(error, res);
        }
    }
}
