import {Response} from "express";
import {ApplicationException} from "../../../../../common/exceptions/application.exception";


export abstract class BaseController {
    handleException(error: any, res: Response) {
        if (error instanceof ApplicationException) {
            const {message, httpCode, internalCode} = error;
            res.status(httpCode);
            res.json({
                message: message,
                code: internalCode
            });
        } else {
            throw new Error(error);
        }
    }
}
