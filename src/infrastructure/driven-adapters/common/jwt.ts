import jwt from "jsonwebtoken";

export class JWT {
    public static sign(id: string) {
        if (process.env.JWT_SECRET_KEY) {
            const secretKey: string = process.env.JWT_SECRET_KEY;
            const expiresIn = 28800
            return {
                token: jwt.sign({id: id}, secretKey, {expiresIn}),
                expiresInSeconds: expiresIn
            } as JWTDto;
        } else {
            throw new Error('Secret key is not defined.');
        }
    }
}
