import bcrypt from 'bcrypt';
import {Hash} from "../../../domain/use-cases/helpers/hash";
import {HashCompare} from "../../../domain/use-cases/helpers/hashCompare";

export class BcryptAdapter implements Hash, HashCompare {

    async hash(text: string): Promise<string> {
        return await bcrypt.hash(text, 10)
    }

    async comparePassword(text: string, digest: string): Promise<boolean> {
        return await bcrypt.compareSync(text, digest);
    }
}
