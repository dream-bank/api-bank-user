import jwt from 'jsonwebtoken'
import {Encrypt} from "../../../domain/use-cases/helpers/encrypt";

export class JwtAdapter implements Encrypt {
    async encrypt(plaintext: string | number): Promise<string> {
        return jwt.sign({account: plaintext},
            JwtAdapter.getJWTSecret(), {expiresIn: JwtAdapter.getJWTExpiresIn()});
    }

    private static getJWTSecret() {
        if (process.env.JWT_SECRET_KEY) {
            return process.env.JWT_SECRET_KEY;
        } else {
            throw new Error('JWT Secret key is not defined.');
        }
    }

    private static getJWTExpiresIn() {
        if (process.env.JWT_SECRET_EXPIRES_IN) {
            return process.env.JWT_SECRET_EXPIRES_IN;
        } else {
            throw new Error('JWT expiresIn is not defined.');
        }
    }
}
