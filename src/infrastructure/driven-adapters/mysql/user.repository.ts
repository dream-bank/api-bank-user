import {UserRepository} from "../../../domain/models/user/gateways/user.repository";
import {User} from "../../../domain/models/user/user";
import connection from "./config/mysql.config";

export class UserRepositoryMySQL implements UserRepository {

    public async createUser(user: User): Promise<void> {
        const {name, surname, identification, password,} = user;
        const currentDate = new Date();
        await connection.execute(
            "INSERT INTO users(name, surname, identification, password, created_at) VALUES(?, ?, ?, ?, ?)",
            [name, surname, identification, password, currentDate]
        );
    }

    public async findUserByIdentification(identification: string): Promise<User | null> {
        const [rows]: any = await connection.execute(
            "SELECT * FROM users WHERE identification = ?",
            [identification]
        );
        if (rows.length) {
            return rows[0] as User;
        }
        return null;
    }
}
