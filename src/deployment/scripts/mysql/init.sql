DROP DATABASE IF EXISTS `bank_users`;
CREATE DATABASE `bank_users`;
USE `bank_users`;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `identification` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL,
    `name`           varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `surname`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `password`       varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
    `created_at`     datetime                                                NOT NULL,
    `updated_at`     datetime DEFAULT NULL,
    PRIMARY KEY (`identification`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `users` (`identification`, `name`, `surname`, `password`, `created_at`, `updated_at`)
VALUES ('1085933225', 'Daniel', 'Burbano', '$2b$10$U4Ngh6ppbZfo3aGlCHVmXOBl1n7pHnv42nIXQXdUzv.naN0kC1BLi',
        '2021-06-19 21:34:07', NULL);
