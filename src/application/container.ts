import express = require("express");
import { createContainer, asClass } from "awilix";
import { scopePerRequest } from "awilix-express";
import {UserUseCase} from "../domain/use-cases/user/user.usecase";
import {UserRepositoryMySQL} from "../infrastructure/driven-adapters/mysql/user.repository";
import {BcryptAdapter} from "../infrastructure/driven-adapters/helpers/bcrypt-adapter";
import {JwtAdapter} from "../infrastructure/driven-adapters/helpers/jwt-adapter";


export default (app: express.Application) => {
  const container = createContainer({
    injectionMode: "CLASSIC",
  });

  container.register({
    //driven adapters
    userRepository: asClass(UserRepositoryMySQL).scoped(),

    //use cases
    userUseCase: asClass(UserUseCase).scoped(),

    //helpers
    hash: asClass(BcryptAdapter).scoped(),
    hashCompare: asClass(BcryptAdapter).scoped(),
    encrypt: asClass(JwtAdapter).scoped()
  });

  app.use(scopePerRequest(container));
};
