import {ApplicationException} from "../../../common/exceptions/application.exception";
import {UserRepository} from "../../models/user/gateways/user.repository";
import {User} from "../../models/user/user";
import {Hash} from "../helpers/hash";
import {HashCompare} from "../helpers/hashCompare";
import {Encrypt} from "../helpers/encrypt";

export class UserUseCase {
    constructor(
        private readonly userRepository: UserRepository,
        private readonly hash: Hash,
        private readonly hashCompare: HashCompare,
        private readonly encrypt: Encrypt,
    ) {
    }

    public async findUserByIdentification(identification: string): Promise<User | null> {
        return await this.userRepository.findUserByIdentification(identification);
    }

    public async createUser(userDto: UserDto): Promise<void> {
        const user = await this.findUserByIdentification(userDto.identification);
        if (!user) {
            userDto.password = await this.hash.hash(userDto.password);
            await this.userRepository.createUser(userDto as User);
            return;
        }
        throw new ApplicationException("User already exists.");
    }

    public async authenticateUser(identification: string, password: string): Promise<JWTDto> {
        const user = await this.findUserByIdentification(identification);
        if(user){
            const isValidPassword = await this.hashCompare.comparePassword(password, user.password);
            if(isValidPassword){
                const accessToken = await this.encrypt.encrypt(user.identification);
                return {
                    token: accessToken,
                    expiresInSeconds: Number(process.env.JWT_SECRET_EXPIRES_IN)
                }
            }
        }
        throw new ApplicationException("Incorrect identification or password", 401,3089);
    }
}
