export interface HashCompare {
    comparePassword: (text: string, digest: string) => Promise<boolean>;
}
