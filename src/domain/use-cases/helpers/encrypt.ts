export interface Encrypt {
    encrypt: (plaintext: string | number) => Promise<string>;
}
