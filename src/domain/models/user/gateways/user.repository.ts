import { User } from "../user";

export interface UserRepository {
  findUserByIdentification(identification: string): Promise<User | null>;
  createUser(user: User): Promise<void>;
}
