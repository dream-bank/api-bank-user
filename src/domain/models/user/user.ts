export interface User {
  identification: string;
  name: string;
  surname: string;
  email: string;
  password: string;
  created_at: Date;
  updated_at: Date | null;
}
