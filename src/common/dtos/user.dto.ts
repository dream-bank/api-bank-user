interface UserDto {
  name: string;
  surname: string;
  email: string;
  identification: string;
  password: string;
}

interface UserAuthDto {
  identification: string;
  password: string;
}
