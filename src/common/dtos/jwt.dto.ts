interface JWTDto {
    token: string,
    expiresInSeconds: number
}
